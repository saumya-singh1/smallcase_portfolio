from flask_restful import Resource
from flask import request, Response
import json
from portfolio_tracker import app, api
import portfolio_tracker.trade_util as trade_util
import portfolio_tracker.portfolio_util as portfolio_util

class Trade(Resource):
    def get(self):
        """
        This route gets all the trades.
        Parameters:
                None
        Returns:
                List of all the trades.
                Example:
                {
                    "trades": [
                        {
                            "id": 2,
                            "security_name": "Tata Consultancy Services Limited",
                            "created_at": "2021-04-05T05:41:01.089314",
                            "trade_date": "2021-04-05T05:41:01.089321",
                            "number_of_shares": 10,
                            "ticker_symbol": "TCS",
                            "trade_type": "BUY",
                            "share_price": 200
                        }
                    ]
                }

        """
        trades = trade_util.get_all_trades()
        resp = Response(response=json.dumps({'trades': trades}),
                        mimetype="application/json")
        return resp

    def post(self):
        """
        This adds the trade and updates the portfolio accordingly.
        Parameters:
                Provide all the mentioned parameters below.
                {
                	"ticker_symbol": "TCS",
                	"trade_type": "SELL",
                	"security_name": "Tata Consultancy Services Limited",
                	"number_of_shares": 10,
                	"share_price": 100.0
                }
        Resturns:
                {
                    "created_at": "2021-04-05T09:26:26.796522",
                    "ticker_symbol": "TCS",
                    "trade_type": "BUY",
                    "share_price": 100,
                    "id": 10,
                    "security_name": "Tata Consultancy Services Limited",
                    "number_of_shares": 10,
                    "trade_date": "2021-04-05T09:26:26.796532"
                }
        """
        data = request.get_json()
        status_code, result = trade_util.post_data(data)
        resp = Response(response=json.dumps(result),
                        status=status_code,
                        mimetype="application/json")
        return resp


    def put(self):
        """
        This update the trade and then update the portfolio accordingly.
        Parameters:
                Takes in 'trade_id' as the query parameter.
                Example:
                    http://127.0.0.1:5000/api/trades?trade_id=5
                Payload:
                    Provide all the mentioned parameters below.
                    {
                    	"ticker_symbol": "TCS",
                    	"trade_type": "SELL",
                    	"security_name": "Tata Consultancy Services Limited",
                    	"number_of_shares": 10,
                    	"share_price": 100.0
                    }
        Returns:
                {
                    "created_at": "2021-04-05T09:26:26.796522",
                    "ticker_symbol": "TCS",
                    "trade_type": "BUY",
                    "share_price": 100,
                    "id": 5,
                    "security_name": "Tata Consultancy Services Limited",
                    "number_of_shares": 10,
                    "trade_date": "2021-04-05T09:26:26.796532"
                }
        """
        data = request.get_json()
        args = request.args
        status_code, result = trade_util.put_data(data, args)
        resp = Response(response=json.dumps(result),
                        status=status_code,
                        mimetype="application/json")
        return resp


    def delete(self):
        """
        This deletes the trade and then update the portfolio accordingly.
        Parameters:
                Takes in 'trade_id' as the query parameter.
                Example:
                    http://127.0.0.1:5000/api/trades?trade_id=5
        Returns:
                {
                    "created_at": "2021-04-05T09:26:26.796522",
                    "ticker_symbol": "TCS",
                    "trade_type": "BUY",
                    "share_price": 100,
                    "id": 5,
                    "security_name": "Tata Consultancy Services Limited",
                    "number_of_shares": 10,
                    "trade_date": "2021-04-05T09:26:26.796532"
                }
        """
        args = request.args
        status_code, result = trade_util.delete_data(args)
        resp = Response(response=json.dumps(result),
                        status=status_code,
                        mimetype="application/json")
        return resp


class Porfolio(Resource):
    def get(self):
        """
        This returns the all the securities in the portfolio.
        Parameters:
                None
        Returns:
                {
                    "portfolio": [
                        {
                            "id": 1,
                            "created_at": "2021-03-05T05:40:16.369439",
                            "ticker_symbol": "WIPRO",
                            "share_quantity": 10,
                            "average_buy_price": 150
                        },
                        {
                            "id": 2,
                            "created_at": "2021-04-05T01:41:01.686686",
                            "ticker_symbol": "TCS",
                            "share_quantity": 9,
                            "average_buy_price": 160
                        }
                    ]
                }
        """
        portfolio = portfolio_util.get_portfolio()
        resp = Response(response=json.dumps({'portfolio': portfolio}),
                        mimetype="application/json")
        return resp


class Return(Resource):
    def get(self):
        """
        This gives the return of a portfolio, assuming current price of any security is Rs. 100.
        Parameters:
                None
        Returns:
            {
                "return": 900
            }
        """
        return_value = portfolio_util.get_return()
        resp = Response(response=json.dumps({'return': return_value}),
                        mimetype="application/json")
        return resp


api.add_resource(Trade, '/api/trades')
api.add_resource(Porfolio, '/api/portfolio')
api.add_resource(Return, '/api/return')
