from portfolio_tracker import db
from portfolio_tracker.model import Portfolio

class PortfolioModel:

    @staticmethod
    def get_portfolio():
        return Portfolio.query.all()


    @staticmethod
    def get_portfolio_by_ticker_symbol(ticker_sym):
        return Portfolio.query.filter_by(ticker_symbol=ticker_sym).first()

    @staticmethod
    def add_security_to_portfolio(ticker_portfolio):
        portfolio = Portfolio(ticker_symbol=ticker_portfolio['ticker_symbol'],
                      average_buy_price=ticker_portfolio['average_buy_price'],
                      share_quantity=ticker_portfolio['share_quantity'])

        db.session.add(portfolio)
        try:
            db.session.commit()
        except Exception as e:
            print(e)
            db.session.rollback()
        db.session.commit()

        return portfolio.id


    @staticmethod
    def update_security_in_portfolio(ticker_portfolio):
        portfolio = Portfolio.query.filter_by(ticker_symbol=ticker_portfolio['ticker_symbol']).first()
        portfolio.average_buy_price = ticker_portfolio['average_buy_price']
        portfolio.share_quantity = ticker_portfolio['share_quantity']
        try:
            db.session.commit()
        except:
            db.session.rollback()
        finally:
            db.session.commit()
