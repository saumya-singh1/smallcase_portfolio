from portfolio_tracker.portfolio_model import PortfolioModel
from portfolio_tracker.model import Portfolio

CURRENT_PRICE_OF_SECURITY = 100

def convert_object_to_dictionary(portfolio_object):
    """
    Convert a trade object provided by SQLAlchemy to Python dictionary.
    """
    portfolio = portfolio_object.__dict__
    del portfolio['_sa_instance_state']
    portfolio['created_at'] = portfolio['created_at'].isoformat()
    return portfolio


def get_portfolio():
    """
    Get all securities from a portfolio.
    """
    portfolio_objects = PortfolioModel.get_portfolio()
    portfolio = []
    for portfolio_object in portfolio_objects:
        portfolio.append(convert_object_to_dictionary(portfolio_object))
    print(portfolio)
    return portfolio


def get_portfolio_info_by_ticker_symbol(ticker_symbol):
    """
    Get securities from a portfolio by its ticker symbol.
    """
    result = {}
    ticker_portfolio = PortfolioModel.get_portfolio_by_ticker_symbol(ticker_symbol)
    if ticker_portfolio:
        result['id'] = ticker_portfolio.id
        result['average_buy_price'] = ticker_portfolio.average_buy_price
        result['share_quantity'] = ticker_portfolio.share_quantity
    else:
        result['id'] = None
        result['average_buy_price'] = 0
        result['share_quantity'] = 0
    return result


def update_portfolio(ticker_symbol, update_avg_buy_price, updates_shares):
    """
    Update portfolio when a trade is updated.
    """
    ticker_portfolio = {}
    ticker_portfolio['ticker_symbol'] = ticker_symbol
    ticker_portfolio['average_buy_price'] = update_avg_buy_price
    ticker_portfolio['share_quantity'] = updates_shares
    PortfolioModel.update_security_in_portfolio(ticker_portfolio)


def upsert_portfolio(data):
    """
    Add a new security in the portfolio when its traded for the first time,
    and update the security otherwise.
    """
    ticker_portfolio = get_portfolio_info_by_ticker_symbol(data['ticker_symbol'])

    if data['trade_type'].upper() == 'BUY':
        new_share_quantity = ticker_portfolio['share_quantity'] + data['number_of_shares']
        ticker_portfolio['average_buy_price'] = ((ticker_portfolio['average_buy_price'] * ticker_portfolio['share_quantity'])
                                                 + (data['number_of_shares'] * data['share_price'])) / new_share_quantity
        ticker_portfolio['share_quantity'] = new_share_quantity

    elif data['trade_type'] == 'SELL':
        ticker_portfolio['share_quantity'] = ticker_portfolio['share_quantity'] - data['number_of_shares']

    ticker_portfolio['ticker_symbol'] = data['ticker_symbol']

    if ticker_portfolio['id'] == None:
        PortfolioModel.add_security_to_portfolio(ticker_portfolio)
    else:
        PortfolioModel.update_security_in_portfolio(ticker_portfolio)


def get_return():
    """
    Get the return amount of the current portfolio.
    """
    total_return = 0
    portfolio = get_portfolio()
    for security in portfolio:
        total_return += (CURRENT_PRICE_OF_SECURITY - security['average_buy_price']) * security['share_quantity']
    return total_return
