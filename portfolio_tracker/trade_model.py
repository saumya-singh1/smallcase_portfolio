from portfolio_tracker import db
from portfolio_tracker.model import Trade

class TradeModel:

    @staticmethod
    def get_all_trades():
        return Trade.query.all()


    @staticmethod
    def get_all_trades_by_ticker_symbol(ticker_symbol):
        return Trade.query.filter_by(ticker_symbol=ticker_symbol)


    @staticmethod
    def get_trade_by_id(trade_id):
        return Trade.query.filter_by(id=trade_id).first()


    @staticmethod
    def add_trade(data):
        trade = Trade(ticker_symbol=data['ticker_symbol'],
                      trade_type=data['trade_type'],
                      security_name=data['security_name'],
                      number_of_shares=data['number_of_shares'],
                      share_price=data['share_price'])

        db.session.add(trade)
        try:
            db.session.commit()
        except Exception as e:
            print(e)
            db.session.rollback()
        db.session.commit()

        return trade.id


    @staticmethod
    def update_trade(data, trade_id):
        trade = Trade.query.filter_by(id=trade_id).first()
        trade.ticker_symbol = data['ticker_symbol']
        trade.trade_type = data['trade_type']
        trade.security_name = data['security_name']
        trade.number_of_shares = data['number_of_shares']
        trade.share_price = data['share_price']
        try:
            db.session.commit()
        except:
            db.session.rollback()
        finally:
            db.session.commit()


    @staticmethod
    def delete_trade(trade_id):
        trade = Trade.query.filter_by(id=trade_id).first()
        db.session.delete(trade)
        try:
            db.session.commit()
        except:
            db.session.rollback()
        finally:
            db.session.commit()
