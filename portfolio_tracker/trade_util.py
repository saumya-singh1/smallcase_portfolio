from http import HTTPStatus
from datetime import datetime
from portfolio_tracker.trade_model import TradeModel
import portfolio_tracker.portfolio_util as portfolio_util
from portfolio_tracker.model import Trade

def validate_request_data(data):
    """
    Validate request payload and return status_code with error response.
    """
    status_code = HTTPStatus.OK
    result = {'Message': 'Request data validation successful.'}

    if data is None or data.get('ticker_symbol') is None or not (data['ticker_symbol']).strip().upper():
        status_code = HTTPStatus.NOT_FOUND
        result = {'Error meassage': 'You need to provide a ticker symbol.'}
    elif data.get('security_name') is None or not (data['security_name']).strip():
        status_code = HTTPStatus.NOT_FOUND
        result = {'Error meassage': 'You need to provide a security name.'}
    elif data.get('trade_type') is None or data.get('number_of_shares') is None or data.get('share_price') is None:
        status_code = HTTPStatus.NOT_FOUND
        result = {'Error meassage': 'You need to provide a trade_type and number_ofshares and share_price.'}
    elif data['trade_type'].strip().upper() not in ['BUY', 'SELL']:
        status_code = HTTPStatus.BAD_REQUEST
        result = {'Error meassage': 'You can only BUY or SELL a security.'}
    elif not isinstance(data['number_of_shares'], int) or not isinstance(data['share_price'], float):
        status_code = HTTPStatus.BAD_REQUEST
        result = {'Error meassage': 'Type of number_of_shares and share_price \
                   should be int and float respectively.'}
    elif data['number_of_shares'] <= 0:
        status_code = HTTPStatus.BAD_REQUEST
        result = {'Error meassage': 'Number of shares should be greater than zero.'}
    elif data['share_price'] <= 0:
        status_code = HTTPStatus.BAD_REQUEST
        result = {'Error meassage': 'Share price should be greater than zero.'}
    return status_code, result


def sanitise_request_data(data):
    """
    Clean up the request data before using it for further computations.
    """
    data['ticker_symbol'] = (data['ticker_symbol']).strip().upper()
    data['security_name'] = (data['security_name']).strip()
    data['trade_type'] = data['trade_type'].strip().upper()
    return data


def validate_query_params(args):
    """
    Validate quer paramets and return the corresponding error code and message.
    """
    status_code = HTTPStatus.OK
    result = {'Message': 'Query param validation successful.'}

    if 'trade_id' not in tuple(args):
        status_code = HTTPStatus.NOT_FOUND
        result = {'Error meassage': 'trade_id should be given as a query param.'}

    elif not args['trade_id'].isnumeric():
        status_code = HTTPStatus.BAD_REQUEST
        result = {'Error meassage': 'trade_id should be an integer.'}
    return status_code, result


def convert_object_to_dictionary(trade_object):
    """
    Convert a trade object provided by SQLAlchemy to Python dictionary.
    """
    trade = trade_object.__dict__
    del trade['_sa_instance_state']
    trade['created_at'] = trade['created_at'].isoformat()
    trade['trade_date'] = trade['trade_date'].isoformat()
    return trade


def get_all_trades_by_ticker_symbol(ticker_symbol):
    """
    Get all trades for the security recognised by its ticker symbol.
    """
    trades_objects = TradeModel.get_all_trades_by_ticker_symbol(ticker_symbol)
    if trades_objects is None:
        return None
    trades = []
    for trade_object in trades_objects:
        trades.append(convert_object_to_dictionary(trade_object))
    return trades


def get_all_trades():
    """
    Get all the trades in the system.
    """
    trades_objects = TradeModel.get_all_trades()
    trades = []
    for trade_object in trades_objects:
        trades.append(convert_object_to_dictionary(trade_object))
    print(trades)
    return trades


def get_trade_by_id(id):
    """
    Get all the trades.
    """
    result = {}
    trade_object = TradeModel.get_trade_by_id(id)
    if trade_object is None:
        return None
    return convert_object_to_dictionary(trade_object)


def validate_add_trade(data):
    """
    Validate if addition of the the given trade is valid.
    """
    status_code = HTTPStatus.OK
    if data['trade_type'] == 'SELL':
        ticker_portfolio = portfolio_util.get_portfolio_info_by_ticker_symbol(data['ticker_symbol'])
        if ticker_portfolio['share_quantity'] - data['number_of_shares'] < 0:
            status_code = HTTPStatus.UNPROCESSABLE_ENTITY
    return status_code


def add_trade(data):
    """
    Add trade to the trade table.
    """
    trade_id = TradeModel.add_trade(data)
    return get_trade_by_id(trade_id)


def sanitise_trades_list(data, trades, trade_id):
    """
    Add the updated trade info to the trades list found by ticker symbol
    so as to calculate the updated average_buy_price and share_quantity.
    """
    trade_to_be_updated = get_trade_by_id(trade_id)
    trade_to_be_updated.update(ticker_symbol = data['ticker_symbol'])
    trade_to_be_updated.update(trade_type = data['trade_type'])
    trade_to_be_updated.update(security_name = data['security_name'])
    trade_to_be_updated.update(number_of_shares = data['number_of_shares'])
    trade_to_be_updated.update(share_price = data['share_price'])

    trades = [trade for trade in trades if not (trade['id'] == trade_id)]
    trades.append(trade_to_be_updated)
    return trades


def validate_update(trades):
    """
    Check if updating the trade is valid or not.
    """
    status_code = HTTPStatus.OK
    share_count = 0
    calculated_avg_buy_price = 0
    for trade in trades:
        if trade['trade_type'].upper() == 'SELL':
            share_count = share_count - trade['number_of_shares']
            if share_count < 0:
                status_code = HTTPStatus.UNPROCESSABLE_ENTITY
                break
        elif trade['trade_type'].upper() == 'BUY':
            total_shares = share_count + trade['number_of_shares']
            calculated_avg_buy_price = (calculated_avg_buy_price * share_count
                + trade['number_of_shares'] * trade['share_price']) / total_shares
            share_count = total_shares
    return status_code, share_count, calculated_avg_buy_price


def get_result(status_code, data, trade_id, update_avg_buy_price, updates_shares):
    """
    Get the response data with respect to status code.
    """
    result = {}
    if status_code == HTTPStatus.UNPROCESSABLE_ENTITY:
        result = {'Error Message': 'Your update is invalid. Final calculated shares cannot be less than zero.'}
    elif status_code == HTTPStatus.OK:
        TradeModel.update_trade(data, trade_id)
        portfolio_util.update_portfolio(data['ticker_symbol'], \
                                        update_avg_buy_price, updates_shares)
        result = get_trade_by_id(trade_id)
    return status_code, result


def validate_and_get_result(trades, data, trade_id):
    """
    Call functions to validate and get the result with respect to various
    status codes.
    """
    sorted_trades = sorted(trades, key = lambda k:k['trade_date'])
    status_code, updated_share_quantity, updated_avg_buy_price = \
                                    validate_update(sorted_trades)
    return get_result(status_code, data, trade_id, updated_avg_buy_price, \
                                            updated_share_quantity)


def update_trade(data, trade_id):
    """
    Checks various validations and update trade and portfoilio.
    """
    trade_to_be_updated = get_trade_by_id(trade_id)
    if trade_to_be_updated is None:
        status_code = HTTPStatus.NOT_FOUND
        result = {'Error Message': 'No such id present in trade table.'}
        return status_code, result

    if data['ticker_symbol'] != trade_to_be_updated['ticker_symbol']:
        # checking shares and average_buy_price for updated or new security
        existing_trade = trade_to_be_updated['ticker_symbol']
        trades = get_all_trades_by_ticker_symbol(data['ticker_symbol'])
        if not trades:
            status_code = HTTPStatus.NOT_FOUND
            result = {'Error Message': 'You cannot add a new security while update. Please add the security first.'}
            return status_code, result
        trades = sanitise_trades_list(data, trades, trade_id)
        sorted_trades = sorted(trades, key = lambda k:k['trade_date'])
        status_code, updated_share_quantity, updated_avg_buy_price = \
                                        validate_update(sorted_trades)
        if status_code == HTTPStatus.UNPROCESSABLE_ENTITY:
            return get_result(status_code, data, trade_id, updated_avg_buy_price, \
                                                updated_share_quantity)
        elif status_code == HTTPStatus.OK:
            # checking shares and average_buy_price for old security
            trades = get_all_trades_by_ticker_symbol(existing_trade)
            trades = [trade for trade in trades if not (trade['id'] == trade_id)]
            return validate_and_get_result(trades, data, trade_id)

    else:
        trades = get_all_trades_by_ticker_symbol(data['ticker_symbol'])
        trades = sanitise_trades_list(data, trades, trade_id)
        return validate_and_get_result(trades, data, trade_id)


def delete_trade(trade_id):
    """
    Delete trade and update portfolio iff deleting trade is validated.
    """
    trade_to_be_deleted = get_trade_by_id(trade_id)
    if trade_to_be_deleted is None:
        status_code = HTTPStatus.NOT_FOUND
        result = {'Error Message': 'No such id present in trade table.'}
        return status_code, result
    trades = get_all_trades_by_ticker_symbol(trade_to_be_deleted['ticker_symbol'])
    trades = [trade for trade in trades if not (trade['id'] == trade_id)]
    sorted_trades = sorted(trades, key = lambda k:k['trade_date'])
    status_code, updated_share_quantity, updated_avg_buy_price = validate_update(sorted_trades)
    result = {}
    if status_code == HTTPStatus.UNPROCESSABLE_ENTITY:
        result = {'Error Message': 'Your delete is invalid. Final calculated \
                   shares cannot be less than zero.'}
    elif status_code == HTTPStatus.OK:
        result = get_trade_by_id(trade_id)
        TradeModel.delete_trade(trade_id)
        portfolio_util.update_portfolio(trade_to_be_deleted['ticker_symbol'], \
                                 updated_avg_buy_price, updated_share_quantity)
    return status_code, result


def post_data(data):
    """
    Call functions to validate addition of trade to portfolio and add it.
    """
    status_code, result = validate_request_data(data)
    if status_code == HTTPStatus.OK:
        data = sanitise_request_data(data)
        status_code = validate_add_trade(data)
        if status_code == HTTPStatus.OK:
            result = add_trade(data)
            portfolio_id = portfolio_util.upsert_portfolio(data)
        elif status_code == HTTPStatus.UNPROCESSABLE_ENTITY:
            result = {'Error Message': 'You cannot sell more shares than you have.'}
    return status_code, result


def put_data(data, args):
    """
    Call functions to validate update of trade and update it.
    """
    status_code, result = validate_request_data(data)
    if status_code == HTTPStatus.OK:
        data = sanitise_request_data(data)
        status_code, result = validate_query_params(args)
        if status_code == HTTPStatus.OK:
            status_code, result = update_trade(data, int(args['trade_id']))
    return status_code, result


def delete_data(args):
    """
    Call functions to validate deletion of trade and delete it.
    """
    status_code, result = validate_query_params(args)
    if status_code == HTTPStatus.OK:
        status_code, result = delete_trade(int(args['trade_id']))
    return status_code, result
