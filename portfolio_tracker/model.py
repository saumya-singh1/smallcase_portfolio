from portfolio_tracker import db
from datetime import date, datetime

class Trade(db.Model):
    """
    Create Trade table via SQLAlchemy.
    """
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    ticker_symbol = db.Column(db.String(100), nullable=False)
    security_name = db.Column(db.String(255), nullable=False)
    trade_type = db.Column(db.String(5), nullable=False)
    number_of_shares = db.Column(db.Integer, nullable=False)
    share_price = db.Column(db.Float, nullable=False)
    trade_date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)


class Portfolio(db.Model):
    """
    Create Portfolio table via SQLAlchemy.
    """
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    ticker_symbol = db.Column(db.String(100), unique=True, nullable=False)
    average_buy_price = db.Column(db.Float, nullable=False)
    share_quantity = db.Column(db.Integer, nullable=False)
