# smallcase_portfolio

Design APIs to track trades, average buy price and share price of a portfolio.

There are 6 APIs provided (APIs are documented in the code itself, in routes.py file)

```
1. GET http://65.2.73.186:5000/api/trades
    - This API gives all the trades that makes up a portfolio.
      Parameters:
                None
        Returns:
                List of all the trades.
                Example:

                {
                    "trades": [
                        {
                            "id": 2,
                            "security_name": "Tata Consultancy Services Limited",
                            "created_at": "2021-04-05T05:41:01.089314",
                            "trade_date": "2021-04-05T05:41:01.089321",
                            "number_of_shares": 10,
                            "ticker_symbol": "TCS",
                            "trade_type": "BUY",
                            "share_price": 200
                        }
                    ]
                }
```


```
2. POST http://65.2.73.186:5000/api/trades
    - This API adds the trade and update the portfolio if shares in portfolio for a security does not come negative.
      Parameters:
                Provide all the mentioned parameters below.

                {
                	"ticker_symbol": "TCS",
                	"trade_type": "SELL",
                	"security_name": "Tata Consultancy Services Limited",
                	"number_of_shares": 10,
                	"share_price": 100.0
                }

        Resturns:

                {
                    "created_at": "2021-04-05T09:26:26.796522",
                    "ticker_symbol": "TCS",
                    "trade_type": "BUY",
                    "share_price": 100,
                    "id": 10,
                    "security_name": "Tata Consultancy Services Limited",
                    "number_of_shares": 10,
                    "trade_date": "2021-04-05T09:26:26.796532"
                }
```


```
3. PUT http://65.2.73.186:5000/api/trades?trade_id=1
    - This API updates the trade and the portfolio if shares in portfolio for a security does not come negative.
      Parameters:
                Takes in 'trade_id' as the query parameter.

                Payload:
                    Provide all the mentioned parameters below.

                    {
                    	"ticker_symbol": "TCS",
                    	"trade_type": "SELL",
                    	"security_name": "Tata Consultancy Services Limited",
                    	"number_of_shares": 10,
                    	"share_price": 100.0
                    }

        Returns:
                {
                    "created_at": "2021-04-05T09:26:26.796522",
                    "ticker_symbol": "TCS",
                    "trade_type": "BUY",
                    "share_price": 100,
                    "id": 5,
                    "security_name": "Tata Consultancy Services Limited",
                    "number_of_shares": 10,
                    "trade_date": "2021-04-05T09:26:26.796532"
                }
 
```
     

```
4. DELETE http://65.2.73.186:5000/api/trades?trade_id=1
    - This API deletes the trade and update the portfolio if shares in portfolio for a security does not come negative.
      Parameters:
                Takes in 'trade_id' as the query parameter.

        Returns:
                {
                    "created_at": "2021-04-05T09:26:26.796522",
                    "ticker_symbol": "TCS",
                    "trade_type": "BUY",
                    "share_price": 100,
                    "id": 5,
                    "security_name": "Tata Consultancy Services Limited",
                    "number_of_shares": 10,
                    "trade_date": "2021-04-05T09:26:26.796532"
                }
```


```
5. GET http://65.2.73.186:5000/api/portfolio
    - This API returns the all the securities in the portfolio.
      Parameters:
                None
      Returns:
                {
                    "portfolio": [
                        {
                            "id": 1,
                            "created_at": "2021-03-05T05:40:16.369439",
                            "ticker_symbol": "WIPRO",
                            "share_quantity": 10,
                            "average_buy_price": 150
                        },
                        {
                            "id": 2,
                            "created_at": "2021-04-05T01:41:01.686686",
                            "ticker_symbol": "TCS",
                            "share_quantity": 9,
                            "average_buy_price": 160
                        }
                    ]
                }
```


```
6. GET http://65.2.73.186:5000/api/return
    - This gives the return of a portfolio, assuming current price of any security is Rs. 100.
      Parameters:
                None
      Returns:
                {
                    "return": 900
                }
```


   
# How calculation works?

For Example - If we have 3 TRADES:

           TRADE_ID |  TICKER  |  PRICE  | SHARE | TYPE
        1. TRADE1   |    TCS   |  100.0  |  10   |  BUY
        2. TRADE2   |    TCS   |  200.0  |  10   |  BUY
        3. TRADE3   |    TCS   |  300.0  |  10   |  SELL

Now, we update TRADE2 from BUY to SELL, then an error will be thrown that this update is not possible as final number of shares are becoming negative.
